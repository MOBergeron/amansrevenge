package ca.qc.cvm.amansrevenge.scene;

import java.util.List;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.amansrevenge.entity.Score;
import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.Interface;
import ca.qc.cvm.amansrevenge.sprite.Interface.InterfaceType;
import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class ScoreScene extends CVMAbstractScene {

	public ScoreScene() {
		super(TextureManager.BG_SCORE, SceneManager.SCORE);
		super.addSprite(new Interface(InterfaceType.TITLE, null));
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float arg0) {
		
	}

	@Override
	public void starting() {
		if(SoundManager.getInstance().isMusic()){
			//
		}
		DataManager.getInstance().openDB();
		CVMText scoreText;
		int y = 110, x = 20;
    	List<Score> scoreList = DataManager.getInstance().fetchScores();
    	for (Score score: scoreList){
    		float time = (int)(score.getTime()/60);
    		time += (float)(score.getTime()%60)/100;
    		scoreText = new CVMText(x, y, 40, time + ": " + score.getScore(), Color.BLACK);
    		this.addText(scoreText);
    		
    		if(y == 410){
    			x+=260;
    			y = 110;
    		}
    		else{
    			y+=50;
    		}
    		
    	}
    	
    	DataManager.getInstance().closeDB();
		
	}
}
