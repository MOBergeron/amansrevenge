package ca.qc.cvm.amansrevenge.scene;

import java.util.Hashtable;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.amansrevenge.entity.Score;
import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.sprite.Interface;
import ca.qc.cvm.amansrevenge.sprite.EnemySprite;
import ca.qc.cvm.amansrevenge.sprite.Interface.InterfaceType;
import ca.qc.cvm.amansrevenge.sprite.EnemySprite.EnemyType;
import ca.qc.cvm.amansrevenge.sprite.GameBGSprite;
import ca.qc.cvm.amansrevenge.sprite.HeroSprite;
import ca.qc.cvm.amansrevenge.sprite.SidePanelBGSprite;
import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class GameScene extends CVMAbstractScene {
	private float mSpawnTimer;
	private float mCountdownTimer;
	private float mPlayTimer;
	private float mSpawnLimit;
	
	private double mNormChanceSpawn;
	private double mAssChanceSpawn;
	private double mTankChanceSpawn;
	
	private int mCombo;
	private int mScore;
	private int mLevel;
	
	private boolean mPause;
	private boolean[] mCountdown = {false, false, false, false};
	
	private CVMText mHPText;
	private CVMText mHeroStateText;
	private CVMText mStateText;
	private CVMText mScoreText;
	private CVMText mPU1Text;
	private CVMText mPU2Text;
	private CVMText mPU3Text;
	private CVMText mCountdownText;
	private CVMText mPauseText;
	private CVMText mEndText;
	
	private Hashtable<Integer, Float> mLevelList;
	
	private HeroSprite mHero;
	private GameBGSprite mBGGame;
	private SidePanelBGSprite mSPBG;
	private Interface mITMoveControl;
	private Interface mITFireControl;
	private Interface mITPause;
	private Interface mITNormal;
	private Interface mITSlush;
	private Interface mITIce;
	private Interface mITIceCream;
	private Interface mITAdrenaline;
	private Interface mITSuperSnow;
	private Interface mCD1;
	private Interface mCD2;
	private Interface mCD3;
	private Interface mCDGO;
	private Interface mTPause;
	private Interface mGameOver;
	
	public GameScene() {
		super(0,0,0, SceneManager.GAME);
		mHero = new HeroSprite();
		mBGGame = new GameBGSprite();
		mSPBG = new SidePanelBGSprite();

		mITMoveControl = new Interface(InterfaceType.MOVECONTROL, mHero);
		mITFireControl = new Interface(InterfaceType.FIRECONTROL, mHero);
		mITPause = new Interface(InterfaceType.PAUSE, null);
		mITNormal = new Interface(InterfaceType.NORMAL, mHero);
		mITSlush = new Interface(InterfaceType.SLUSH, mHero);
		mITIce = new Interface(InterfaceType.ICE, mHero);
		mITIceCream = new Interface(InterfaceType.ICECREAM, null);
		mITAdrenaline = new Interface(InterfaceType.ADRENALINE, null);
		mITSuperSnow = new Interface(InterfaceType.SUPERSNOW, null);
		mCD1 = new Interface(InterfaceType.CD1, null);
		mCD2 = new Interface(InterfaceType.CD2, null);
		mCD3 = new Interface(InterfaceType.CD3, null);
		mCDGO = new Interface(InterfaceType.CDGO, null);
		mTPause = new Interface(InterfaceType.T_PAUSE, null);
		mGameOver = new Interface(InterfaceType.GAMEOVER, null);
		
		mCountdownText = new CVMText(0, 0, 70," 3! ", Color.BLACK);
		mPauseText = new CVMText(0, 0, 90, "PAUSE", Color.BLACK);
		mStateText = new CVMText(0, 0, 22, "State:", Color.BLACK);
		mHeroStateText = new CVMText(0, 0, 22, "            ", Color.BLACK);
		mScoreText = new CVMText(0, 0, 22, "Score: 00000000 ", Color.BLACK);
		mHPText = new CVMText(0, 0, 22, "Health: 100 ", Color.BLACK);
		mPU1Text = new CVMText(0, 0, 22, "x0 ", Color.BLACK);
		mPU2Text = new CVMText(0, 0, 22, "x0 ", Color.BLACK);
		mPU3Text = new CVMText(0, 0, 22, "x0 ", Color.BLACK);
		mEndText = new CVMText(0, 0, 78, "Game Over", Color.BLACK);
		
		mLevelList = new Hashtable<Integer, Float>();
		mLevelList.put(1, (float) 60);
		mLevelList.put(2, (float) 120);
		mLevelList.put(3, (float) 170); 
		mLevelList.put(4, (float) 205);
		mLevelList.put(5, (float) 250);
		mLevelList.put(6, (float) 290);
		mLevelList.put(7, (float) 330);
		mLevelList.put(8, (float) 365);
		mLevelList.put(9, (float) 400);
		mLevelList.put(10, (float) 430);

	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float elapsedseconds) {
		if(StateManager.getInstance().getState() == StateManager.WAIT){
			waitingStatement(elapsedseconds);
		}
		else if(StateManager.getInstance().getState() == StateManager.PLAY){
			playStatement(elapsedseconds);
		}
		else if(StateManager.getInstance().getState() == StateManager.PAUSE){
			pauseStatement();
		}
		else if(StateManager.getInstance().getState() == StateManager.END){
			endStatement(elapsedseconds);
		}
	}

	@Override
	public void starting() {
		StateManager.getInstance().setState(StateManager.WAIT);
		
		if(SoundManager.getInstance().isMusic()){
			SoundManager.getInstance().playMusic("musics/NowOrNever.mp3", this.gameActivity);
		}
		
		mHero.restart();

		super.addSprite(mHero);
		super.addSprite(mBGGame);
		super.addSprite(mSPBG);
		super.addSprite(mITMoveControl);
		super.addSprite(mITFireControl);
		super.addSprite(mITPause);
		
		if(DataManager.getInstance().isNormalBall()){
			super.addSprite(mITNormal);
		}
		if(DataManager.getInstance().isSlush()){
			super.addSprite(mITSlush);
		}
		if(DataManager.getInstance().isIce()){
			super.addSprite(mITIce);
		}
		
		super.addSprite(mITIceCream);
		super.addSprite(mITAdrenaline);
		super.addSprite(mITSuperSnow);
		
		for(int i=0; i<mCountdown.length; i++){
			mCountdown[i] = false;	
		}
		
		mHero.getSprite().setZIndex(-1);
		mBGGame.getSprite().setZIndex(-2);
		this.sortChildren();
		
		mPause = false;
		mCountdownTimer = 0;
		mScore = 0;
		mCombo = 1;
		mPlayTimer = 0;
		mSpawnLimit = 2;
		mSpawnTimer = mSpawnLimit;
		mLevel = 1;
		
		boolean normal = true;
		boolean assassin = true;
		boolean tank = true;
		
		mNormChanceSpawn = EnemyType.NORMAL.getChance();
		mAssChanceSpawn  = EnemyType.ASSASSIN.getChance();
		mTankChanceSpawn = EnemyType.TANK.getChance();
		
		if(!DataManager.getInstance().isNormalMan()){
			normal = false;
		}
		if(!DataManager.getInstance().isAssassin()){
			assassin = false;
		}
		if(!DataManager.getInstance().isTank()){
			tank = false;
		}
		
		if(normal && (!assassin || !tank)){
			if(!assassin && !tank){
				mNormChanceSpawn = 1;
				mAssChanceSpawn = -1;
				mTankChanceSpawn = -1;
			}
			else if(!assassin){
				mNormChanceSpawn = 1;
				mAssChanceSpawn = 0.35;
				mTankChanceSpawn = 0.35;
			}
			else if(!tank){
				mNormChanceSpawn = 1;
				mAssChanceSpawn = 0.4;
				mTankChanceSpawn = -1;
			}
		}
		else if(assassin && (!normal || !tank)){
			if(!normal && !tank){
				mNormChanceSpawn = -1;
				mAssChanceSpawn = 1;
				mTankChanceSpawn = -1;
			}
			else if(!normal){
				mNormChanceSpawn = -1;
				mAssChanceSpawn = 1;
				mTankChanceSpawn = 0.45;
			}
			else if(!tank){
				mNormChanceSpawn = 1;
				mAssChanceSpawn = 0.4;
				mTankChanceSpawn = -1;
			}
		}
		else if(tank && (!normal || !assassin)){
			if(!normal && !assassin){
				mNormChanceSpawn = -1;
				mAssChanceSpawn = -1;
				mTankChanceSpawn = 1;
			}
			else if(!normal){
				mNormChanceSpawn = -1;
				mAssChanceSpawn = 1;
				mTankChanceSpawn = 0.45;
			}
			else if(!assassin){
				mNormChanceSpawn = 1;
				mAssChanceSpawn = 0.35;
				mTankChanceSpawn = 0.35;
			}
		}
		
		super.addText(mStateText);
		super.addText(mHeroStateText);
		super.addText(mHPText);
		super.addText(mScoreText);
		super.addText(mPU1Text);
		super.addText(mPU2Text);
		super.addText(mPU3Text);
		super.addSprite(mCD3);
	}
	
	public void waitingStatement(float elapsedseconds){
		// Countdown avant de jouer (3..., 2..., 1..., GO...)
		mCountdownTimer += elapsedseconds;
		
		if((mCountdownTimer >= 0 && mCountdownTimer < 1) ){
			if(mPause){
				super.addSprite(mCD3);
				super.removeSprite(mTPause);
				
				mPause = false;
			}
			mCD3.update(this.gameActivity);
			mCountdown[0] = true;
		}
		else if((mCountdownTimer >= 1 && mCountdownTimer < 2)){
			if(!mCountdown[1]){
				super.addSprite(mCD2);
				super.removeSprite(mCD3);
				mCD2.update(this.gameActivity);
				mCountdown[1] = true;
			}
		}
		else if((mCountdownTimer >= 2 && mCountdownTimer < 3) ){
			if(!mCountdown[2]){
				super.addSprite(mCD1);
				super.removeSprite(mCD2);
				mCD1.update(this.gameActivity);
				mCountdown[2] = true;
			}
		}
		else if((mCountdownTimer >= 3 && mCountdownTimer < 4) ){
			if(!mCountdown[3]){
				super.addSprite(mCDGO);
				super.removeSprite(mCD1);
				mCDGO.update(this.gameActivity);
				mCountdown[3] = true;
			}
		}
		else if(mCountdownTimer >= 4){
			StateManager.getInstance().setState(StateManager.PLAY);
			super.removeSprite(mCDGO);
			for(int i=0; i<mCountdown.length; i++){
				mCountdown[i] = false;	
			}
		}
	}
	
	public void playStatement(float elaspedSeconds){
		if(!mHero.isAlive()){
			StateManager.getInstance().setState(StateManager.END);
			mCountdownTimer = 0;
		}
		else{
			mSpawnTimer -= elaspedSeconds;
			mPlayTimer += elaspedSeconds;
			
			if(mSpawnTimer <= 0){
				mSpawnTimer = mSpawnLimit - mSpawnTimer;
				double random = Math.random();
				if(random <= mNormChanceSpawn && random > mAssChanceSpawn){
					super.addSprite(new EnemySprite(mHero, EnemyType.NORMAL));
				}
				else if(random <= mAssChanceSpawn && random > mTankChanceSpawn){
					super.addSprite(new EnemySprite(mHero, EnemyType.ASSASSIN));
				}
				else if(random <= mTankChanceSpawn && random >= 0){
					super.addSprite(new EnemySprite(mHero, EnemyType.TANK));
				}
				
			}
			
			if(mLevel <= 10 && mPlayTimer >= mLevelList.get(mLevel)){
				mSpawnLimit -= 0.125;
				mLevel++;
			}
		}
	}
	
	public void pauseStatement(){
		if(!mPause){
			mPause = true;

			super.addSprite(mTPause);
			mTPause.update(this.gameActivity);
			mCountdownTimer = 0;
			
			if(mCountdown[3]){
				super.removeSprite(mCDGO);
			}
			else if(mCountdown[2]){
				super.removeSprite(mCD1);
			}
			else if(mCountdown[1]){
				super.removeSprite(mCD2);
			}
			else if(mCountdown[0]){
				super.removeSprite(mCD3);
			}
			
			for(int i=0; i<mCountdown.length; i++){
				mCountdown[i] = false;	
			}
		}
	}
	
	public void endStatement(float elaspedSeconds){
		super.addSprite(mGameOver);
		mGameOver.update(this.gameActivity);
		if(mCountdownTimer == 0){
			DataManager.getInstance().openDB();
			DataManager.getInstance().addScore(new Score(0, (int)mPlayTimer, mScore));
			DataManager.getInstance().closeDB();
		}
		else if(mCountdownTimer > 3){
			this.gameActivity.onBackPressed();
    	}
    	
    	mCountdownTimer+=elaspedSeconds;
	}
	
	public void refreshText(){
		if(!mHeroStateText.getDisplayText().equals(mHero.getState())){
			mHeroStateText.getText().setText(mHero.getState());
		}
		
		mHPText.getText().setText("Health: " + mHero.getHealth());
		mPU1Text.getText().setText("x" + mHero.getIceCreamCount());
		mPU2Text.getText().setText("x" + mHero.getAdrenalineCount());
		mPU3Text.getText().setText("x" + mHero.getSuperSnowCount());
		mScoreText.getText().setText("Score: " + mScore);
		
		mStateText.setPosition(gameActivity.getCamera().getXMin()+330, gameActivity.getCamera().getYMax()-30);
		mHeroStateText.setPosition(gameActivity.getCamera().getXMin()+400, gameActivity.getCamera().getYMax()-30);
		mCountdownText.setPosition(gameActivity.getCamera().getXMin()+330, gameActivity.getCamera().getYMin()+195);
		mPauseText.setPosition(gameActivity.getCamera().getXMin()+255, gameActivity.getCamera().getYMin()+192);
		mHPText.setPosition(gameActivity.getCamera().getXMin()+85, gameActivity.getCamera().getYMin()+10);
		mScoreText.setPosition(gameActivity.getCamera().getXMax()-200, gameActivity.getCamera().getYMin()+10);
		mPU1Text.setPosition(gameActivity.getCamera().getXMin()+45, gameActivity.getCamera().getYMin()+275);
		mPU2Text.setPosition(gameActivity.getCamera().getXMin()+45, gameActivity.getCamera().getYMin()+340);
		mPU3Text.setPosition(gameActivity.getCamera().getXMin()+45, gameActivity.getCamera().getYMin()+405);
		mEndText.setPosition(gameActivity.getCamera().getXMin()+255, gameActivity.getCamera().getYMin()+192);
	}

	public void lowCombo(int combo){
		this.mCombo-=combo;
	}
	
	public void raiseCombo(int combo){
		this.mCombo+=combo;
	}
	
	public void setScore(int score){
		this.mScore += score*mCombo;
	}
	
	public void update(){
		mSPBG.update(this.gameActivity);
		mITMoveControl.update(this.gameActivity);
		mITFireControl.update(this.gameActivity);
		mITPause.update(this.gameActivity);

		if(DataManager.getInstance().isNormalBall()){
			mITNormal.update(this.gameActivity);
		}
		if(DataManager.getInstance().isSlush()){
			mITSlush.update(this.gameActivity);
		}
		if(DataManager.getInstance().isIce()){
			mITIce.update(this.gameActivity);
		}
		
		mITIceCream.update(this.gameActivity);
		mITAdrenaline.update(this.gameActivity);
		mITSuperSnow.update(this.gameActivity);
	}
}
