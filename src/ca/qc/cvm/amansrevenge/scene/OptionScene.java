package ca.qc.cvm.amansrevenge.scene;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.ButtonOff;
import ca.qc.cvm.amansrevenge.sprite.ButtonOn;
import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class OptionScene extends CVMAbstractScene {

	public OptionScene() {
		super(TextureManager.BG_OPTION, SceneManager.OPTION);
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float arg0) {
		
	}

	@Override
	public void starting() {
		DataManager.getInstance().openDB();
		DataManager.getInstance().loadAll();
		DataManager.getInstance().closeDB();
		
		int firstColumn = 20;
		float secondColumn = 210;
		int thirdColumn = 360;
		float forthColumn = 650;
		float row = 50;
		
		super.addText(new CVMText(firstColumn, (int)row+30, 40,"Music", Color.BLACK));
		if(SoundManager.getInstance().isMusic()){
			super.addSprite(new ButtonOn(secondColumn, row+30, DataManager.MUSIC));
		}
		else{
			super.addSprite(new ButtonOff(secondColumn, row+30, DataManager.MUSIC));
		}
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Adrenaline", Color.BLACK));
		if(DataManager.getInstance().isAdrenaline()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.ADRENALINE));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.ADRENALINE));
		}
		
		row += 60;
		
		super.addText(new CVMText(firstColumn, (int)row+30, 40,"Sound", Color.BLACK));
		if(SoundManager.getInstance().isSound()){
			super.addSprite(new ButtonOn(secondColumn, row+30, DataManager.SOUND));
		}
		else{
			super.addSprite(new ButtonOff(secondColumn, row+30, DataManager.SOUND));
		}
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Ice Cream", Color.BLACK));
		if(DataManager.getInstance().isIcecream()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.ICECREAM));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.ICECREAM));
		}
		
		row += 60;
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Super Snow", Color.BLACK));
		if(DataManager.getInstance().isSupersnow()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.SUPERSNOW));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.SUPERSNOW));
		}
		
		row += 120;
		
		super.addText(new CVMText(firstColumn, (int)row, 40,"Normal", Color.BLACK));
		if(DataManager.getInstance().isNormalBall()){
			super.addSprite(new ButtonOn(secondColumn, row, DataManager.NORMAL_BALL));
		}
		else{
			super.addSprite(new ButtonOff(secondColumn, row, DataManager.NORMAL_BALL));
		}
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Normal", Color.BLACK));
		if(DataManager.getInstance().isNormalMan()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.NORMAL_MAN));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.NORMAL_MAN));
		}
		
		row += 60;
		
		super.addText(new CVMText(firstColumn, (int)row, 40,"Slush", Color.BLACK));
		if(DataManager.getInstance().isSlush()){
			super.addSprite(new ButtonOn(secondColumn, row, DataManager.SLUSH));
		}
		else{
			super.addSprite(new ButtonOff(secondColumn, row, DataManager.SLUSH));
		}
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Assassin", Color.BLACK));
		if(DataManager.getInstance().isAssassin()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.ASSASSIN));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.ASSASSIN));
		}
		
		row += 60;
		
		super.addText(new CVMText(firstColumn, (int)row, 40,"Ice", Color.BLACK));
		if(DataManager.getInstance().isIce()){
			super.addSprite(new ButtonOn(secondColumn, row, DataManager.ICE));
		}
		else{
			super.addSprite(new ButtonOff(secondColumn, row, DataManager.ICE));
		}
		
		super.addText(new CVMText(thirdColumn, (int)row, 40,"Tank", Color.BLACK));
		if(DataManager.getInstance().isTank()){
			super.addSprite(new ButtonOn(forthColumn, row, DataManager.TANK));
		}
		else{
			super.addSprite(new ButtonOff(forthColumn, row, DataManager.TANK));
		}
	}
}
