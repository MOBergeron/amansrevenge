package ca.qc.cvm.amansrevenge.scene;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.Interface;
import ca.qc.cvm.amansrevenge.sprite.Interface.InterfaceType;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class SplashScene extends CVMAbstractScene {

	public SplashScene() {
		super(TextureManager.BG_SPLASH, SceneManager.SPLASH);
		super.addSprite(new Interface(InterfaceType.TITLE, null));
		super.addSprite(new Interface(InterfaceType.PLAY, null));
		super.addSprite(new Interface(InterfaceType.OPTION, null));
		super.addSprite(new Interface(InterfaceType.SCORE, null));
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float arg0) {
		
	}

	@Override
	public void starting() {
		StateManager.getInstance().setState(StateManager.MENU);
		DataManager.getInstance().openDB();
		DataManager.getInstance().loadAll();
		
		if(!DataManager.getInstance().isNormalBall() && !DataManager.getInstance().isSlush() && !DataManager.getInstance().isIce()){
			DataManager.getInstance().setOption(DataManager.NORMAL_BALL, true);
		}
		
		if(!DataManager.getInstance().isNormalMan() && !DataManager.getInstance().isAssassin() && !DataManager.getInstance().isTank()){
			DataManager.getInstance().setOption(DataManager.NORMAL_MAN, true);
		}

		DataManager.getInstance().loadAll();
		DataManager.getInstance().closeDB();
		
		if(SoundManager.getInstance().isMusic()){
			SoundManager.getInstance().playMusic("musics/FindYou.mp3", this.gameActivity);
		}
		
		this.gameActivity.getCamera().setXMin(0);
		this.gameActivity.getCamera().setYMin(0);
		this.gameActivity.getCamera().setXMax(800);
		this.gameActivity.getCamera().setYMax(480);
	}
}
