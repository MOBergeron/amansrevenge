package ca.qc.cvm.amansrevenge.singleton;

import ca.qc.cvm.cvmandengine.CVMSoundManager;
import ca.qc.cvm.cvmandengine.entity.CVMSound;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class SoundManager extends CVMSoundManager {
	public static final int THROW = 1;
	public static final int DIE = 2;
	public static final int HURT = 3;
	public static final int TOUCHED = 4;
	public static final int ICECREAM = 5;
	public static final int ADRENALINE = 6;
	public static final int SUPERSNOW = 7;
	public static final int EXHAUSTED = 8;
	public static final int INVINCIBLE = 9;
	public static final int BRAINFREEZE = 10;
	
	private boolean music;
	private boolean sound;
	
	private String path;
	
	private CVMGameActivity activity;
	
	private static SoundManager instance;
	
	private SoundManager() {
		// Cr�ation du son explosion, qui est maintenant disponible via la m�thode : super.playSound(SoundMng.EXPLOSION)
		super.addSound(new CVMSound(THROW, "sounds/throw.mp3"));
		super.addSound(new CVMSound(DIE, "sounds/die.wav"));
		super.addSound(new CVMSound(HURT, "sounds/hurt.mp3"));
		super.addSound(new CVMSound(TOUCHED, "sounds/touched.mp3"));
		super.addSound(new CVMSound(ICECREAM, "sounds/icecream.mp3"));
		super.addSound(new CVMSound(ADRENALINE, "sounds/adrenaline.mp3"));
		super.addSound(new CVMSound(SUPERSNOW, "sounds/supersnow.mp3"));
		super.addSound(new CVMSound(EXHAUSTED, "sounds/exhausted.mp3"));
		super.addSound(new CVMSound(INVINCIBLE, "sounds/invincible.mp3"));
		super.addSound(new CVMSound(BRAINFREEZE, "sounds/brainfreeze.mp3"));
	}
	
	public static SoundManager getInstance() {
		if (instance == null) {
			instance = new SoundManager();
		}
		
		return instance;
	}
	
	public void play(int soundId){
		if(sound){
			super.playSound(soundId);
		}
	}
	
	public void playMusic(String path, CVMGameActivity activity){
		activity.setMusic(path);
		this.path = path;
	}

	public boolean isMusic() {
		return music;
	}

	public boolean isSound() {
		return sound;
	}
	
	public void setActivity(CVMGameActivity activity){
		this.activity = activity;
	}

	public void setMusic(boolean music) {
		this.music = music;
		if(!music){
			activity.stopMusic();
		}
		else{
			activity.setMusic(path);
		}
	}

	public void setSound(boolean sound) {
		this.sound = sound;
	}
}
