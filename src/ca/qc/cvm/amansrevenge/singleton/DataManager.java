package ca.qc.cvm.amansrevenge.singleton;

import java.util.List;

import ca.qc.cvm.amansrevenge.entity.Score;
import ca.qc.cvm.amansrevenge.entity.User;
import ca.qc.cvm.amansrevenge.model.Model;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class DataManager {
	public static String ADRENALINE = "adrenaline";
	public static String ASSASSIN = "assassin";
	public static String ICE = "ice";
	public static String ICECREAM = "icecream";
	public static String MUSIC = "music";
	public static String NORMAL_BALL = "normal_ball";
	public static String NORMAL_MAN = "normal_man";
	public static String SLUSH = "slush";
	public static String SOUND = "sound";
	public static String SUPERSNOW = "supersnow";
	public static String TANK = "tank";
	
	private boolean mAdrenaline;
	private boolean mIcecream;
	private boolean mSupersnow;
	private boolean mNormalBall;
	private boolean mSlush;
	private boolean mIce;
	private boolean mNormalMan;
	private boolean mAssassin;
	private boolean mTank;
	
	private static DataManager instance;
	private Model model;
	
	private DataManager() {
	}
	
	public void addScore(Score score){
		model.addScore(score);
	}
	
	public void closeDB(){
		model.closeDatabase();
	}
	
	public List<Score> fetchScores(){
		return model.fetchScores();
	}
	
	public boolean isAdrenaline() {
		return mAdrenaline;
	}

	public boolean isAssassin() {
		return mAssassin;
	}

	public boolean isIce() {
		return mIce;
	}

	public boolean isIcecream() {
		return mIcecream;
	}

	public boolean isNormalBall() {
		return mNormalBall;
	}

	public boolean isNormalMan() {
		return mNormalMan;
	}

	public boolean isSlush() {
		return mSlush;
	}

	public boolean isSupersnow() {
		return mSupersnow;
	}

	public boolean isTank() {
		return mTank;
	}

	public void initModel(CVMGameActivity activity){
		model = new Model(activity);
	}
	
	public void loadAll(){
		User user = model.fetchUser();
		
		if(user.getAdrenaline() == User.TRUE){
			mAdrenaline = true;
		}
		else{
			mAdrenaline = false;
		}
		
		if(user.getAssassin() == User.TRUE){
			mAssassin = true;
		}
		else{
			mAssassin = false;
		}
		
		if(user.getIce() == User.TRUE){
			mIce = true;
		}
		else{
			mIce = false;
		}
		
		if(user.getIcecream() == User.TRUE){
			mIcecream = true;
		}
		else{
			mIcecream = false;
		}
		
		if(user.getMusic() == User.TRUE){
			SoundManager.getInstance().setMusic(true);
		}
		else{
			SoundManager.getInstance().setMusic(false);
		}
		
		if(user.getNormal_ball() == User.TRUE){
			mNormalBall = true;
		}
		else{
			mNormalBall = false;
		}
		
		if(user.getNormal_man() == User.TRUE){
			mNormalMan = true;
		}
		else{
			mNormalMan = false;
		}
		
		if(user.getSlush() == User.TRUE){
			mSlush = true;
		}
		else{
			mSlush = false;
		}
		
		if(user.getSound() == User.TRUE){
			SoundManager.getInstance().setSound(true);
		}
		else{
			SoundManager.getInstance().setSound(false);
		}
		
		if(user.getSupersnow() == User.TRUE){
			mSupersnow = true;
		}
		else{
			mSupersnow = false;
		}
		
		if(user.getTank() == User.TRUE){
			mTank = true;
		}
		else{
			mTank = false;
		}
	}
	
	public void loadMusic(){
		long[] list = model.getMusic();
		
		if(list[0] == User.TRUE){
			SoundManager.getInstance().setMusic(true);
		}
		else{
			SoundManager.getInstance().setMusic(false);
		}
	}
	
	public void loadSound(){
		long[] list = model.getSound();
		
		if(list[0] == User.TRUE){
			SoundManager.getInstance().setSound(true);
		}
		else{
			SoundManager.getInstance().setSound(false);
		}
	}
	
	public void openDB(){
		model.openDatabase();
	}
	
	public void setOption(String link, boolean value){
		model.setOption(link, value);
		if(link == MUSIC){
			loadMusic();
		}
		else if(link == SOUND){
			loadSound();
		}
	}
	
	public static DataManager getInstance() {
		if (instance == null) {
			instance = new DataManager();
		}
		
		return instance;
	}
}