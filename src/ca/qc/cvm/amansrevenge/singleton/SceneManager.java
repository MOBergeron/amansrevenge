package ca.qc.cvm.amansrevenge.singleton;

public class SceneManager {
	public static final int NONE = 0;
	public static final int SPLASH = 1;
	public static final int GAME = 2;
	public static final int OPTION = 3;
	public static final int SCORE = 4;
	
	private SceneManager() {}
}