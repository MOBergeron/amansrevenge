package ca.qc.cvm.amansrevenge.singleton;

public class StateManager {
	public static final int UNINITIALIZE = 0;
	public static final int MENU = 1;
	public static final int PLAY = 2;
	public static final int PAUSE = 3;
	public static final int WAIT = 4;
	public static final int END = 5;
	
	private int mState;
	
	private static StateManager instance;
	
	private StateManager() {
		mState = UNINITIALIZE;
	}
	
	public static StateManager getInstance() {
		if (instance == null) {
			instance = new StateManager();
		}
		
		return instance;
	}

	public int getState() {
		return mState;
	}

	public void setState(int mState) {
		this.mState = mState;
	}
}