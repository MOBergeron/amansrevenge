package ca.qc.cvm.amansrevenge.singleton;

import ca.qc.cvm.cvmandengine.CVMTextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMTexture;

public class TextureManager extends CVMTextureManager {
	//=================================================
	// Sprite
	//=================================================
	public static final int S_HERO = 1;
	public static final int S_SNOWMAN = 2;
	public static final int S_SNOWMAN_T = 3;
	public static final int S_SNOWMAN_A = 4;
	public static final int S_SNOWBALL = 5;
	public static final int S_SLUSHBALL = 6;
	public static final int S_ICEBALL = 7;
	public static final int S_PU_ICECREAM = 8;
	public static final int S_PU_ADRENALINE = 9;
	public static final int S_PU_SUPERSNOW = 10;
	public static final int S_CONTROL = 11;
	public static final int S_TITLE = 12;
	public static final int S_PLAY = 13;
	public static final int S_OPTION = 14;
	public static final int S_SCORE = 15;
	public static final int S_PAUSE = 16;
	public static final int S_ON = 17;
	public static final int S_OFF = 18;
	public static final int SP_SNOWBALL = 19;
	public static final int SP_SLUSHBALL = 20;
	public static final int SP_ICEBALL = 21;
	public static final int T_CD3 = 22;
	public static final int T_CD2 = 23;
	public static final int T_CD1 = 24;
	public static final int T_CDGO = 25;
	public static final int T_PAUSE = 26;
	public static final int T_GAMEOVER = 27;
	
	public static final int POINT_PARTICULE = 50;
	
	//=================================================
	// Background
	//=================================================
	public static final String BG_SPLASH = "backgrounds/splashbg.png";
	public static final String BG_OPTION = "backgrounds/optionbg.png";
	public static final String BG_SCORE = "backgrounds/scorebg.png";
	public static final int BG_GAME = 100;
	public static final int BG_SIDEPANEL = 101;
	
	private static TextureManager instance;
	
	private TextureManager() {
		// Cr�ation des ressources
		super.addTexture(new CVMTexture(S_HERO, "sprites/hero2.png", 50, 30));
		super.addTexture(new CVMTexture(S_SNOWMAN, "sprites/normal.png", 36, 47));
		super.addTexture(new CVMTexture(S_SNOWMAN_T, "sprites/tanksheet.png", 160, 40, 4, 1));
		super.addTexture(new CVMTexture(S_SNOWMAN_A, "sprites/assassinsheet.png", 178, 36, 4, 1));
		super.addTexture(new CVMTexture(S_SNOWBALL, "sprites/snowball.png", 12, 20));
		super.addTexture(new CVMTexture(S_SLUSHBALL, "sprites/slushball.png", 12, 20));
		super.addTexture(new CVMTexture(S_ICEBALL, "sprites/iceball.png", 12, 20));
		super.addTexture(new CVMTexture(S_PU_ICECREAM, "sprites/icecream.png", 56, 28, 2, 1));
		super.addTexture(new CVMTexture(S_PU_ADRENALINE, "sprites/adrenaline.png", 57, 28, 2, 1));
		super.addTexture(new CVMTexture(S_PU_SUPERSNOW, "sprites/supersnow.png", 56, 28, 2, 1));
		super.addTexture(new CVMTexture(S_CONTROL, "sprites/control.png", 200, 200));
		super.addTexture(new CVMTexture(S_TITLE, "sprites/title1.png", 737, 81));
		super.addTexture(new CVMTexture(S_PLAY, "sprites/play1.png", 297, 66));
		super.addTexture(new CVMTexture(S_OPTION, "sprites/option1.png", 383, 67));
		super.addTexture(new CVMTexture(S_SCORE, "sprites/score1.png", 362, 66));
		super.addTexture(new CVMTexture(S_PAUSE, "sprites/pause.png", 45, 45));
		super.addTexture(new CVMTexture(S_ON, "sprites/on.png", 129, 79));
		super.addTexture(new CVMTexture(S_OFF, "sprites/off.png", 129, 79));
		super.addTexture(new CVMTexture(POINT_PARTICULE, "particles/particle_point.png", 32, 32));
		super.addTexture(new CVMTexture(BG_GAME, "backgrounds/gamebg.png", 2000, 1200));
		super.addTexture(new CVMTexture(BG_SIDEPANEL, "backgrounds/sidepanel.png", 80, 480));
		super.addTexture(new CVMTexture(SP_SNOWBALL, "sprites/snowball_sp.png", 45, 45));
		super.addTexture(new CVMTexture(SP_SLUSHBALL, "sprites/slushball_sp.png", 45, 45));
		super.addTexture(new CVMTexture(SP_ICEBALL, "sprites/iceball_sp.png", 45, 45));
		super.addTexture(new CVMTexture(T_CD3, "sprites/countdown3.png", 147, 71));
		super.addTexture(new CVMTexture(T_CD2, "sprites/countdown2.png", 147, 71));
		super.addTexture(new CVMTexture(T_CD1, "sprites/countdown1.png", 147, 71));
		super.addTexture(new CVMTexture(T_CDGO, "sprites/go.png", 147, 73));
		super.addTexture(new CVMTexture(T_PAUSE, "sprites/t_pause.png", 362, 66));
		super.addTexture(new CVMTexture(T_GAMEOVER, "sprites/GameOver.png", 494, 71));
		
	}
	
	public static TextureManager getInstance() {
		if (instance == null) {
			instance = new TextureManager();
		}
		
		return instance;
	}
}
