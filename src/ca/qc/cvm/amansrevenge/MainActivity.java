package ca.qc.cvm.amansrevenge;

import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.amansrevenge.scene.GameScene;
import ca.qc.cvm.amansrevenge.scene.OptionScene;
import ca.qc.cvm.amansrevenge.scene.ScoreScene;
import ca.qc.cvm.amansrevenge.scene.SplashScene;
import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class MainActivity extends CVMGameActivity {

	public MainActivity() {
		super(TextureManager.getInstance());
		
		super.setSoundManager(SoundManager.getInstance());
		SoundManager.getInstance().setActivity(this);
		DataManager.getInstance().initModel(this);
		
		List<CVMAbstractScene> sceneList = new ArrayList<CVMAbstractScene>();
		
		sceneList.add(new SplashScene());
		sceneList.add(new GameScene());
		sceneList.add(new OptionScene());
		sceneList.add(new ScoreScene());
		
		super.setSceneList(sceneList);
	}

	@Override
	public void backPressed() {
		switch(getCurrentSceneId()){
		case SceneManager.SPLASH: 
			this.finish();
			break;
		case SceneManager.GAME: 
			this.changeScene(SceneManager.SPLASH);
			break;
		case SceneManager.OPTION: 
			this.changeScene(SceneManager.SPLASH);
			break;
		case SceneManager.SCORE: 
			this.changeScene(SceneManager.SPLASH);
			break;
		default: this.finish();
			break;
		}	
	}
}
