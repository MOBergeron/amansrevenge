package ca.qc.cvm.amansrevenge.entity;

public class User {
	public static final int TRUE = 1;
	public static final int FALSE = 0;
	
	private int id;
	private int music;
	private int sound;
	private int adrenaline;
	private int icecream;
	private int supersnow;
	private int normal_ball;
	private int slush;
	private int ice;
	private int normal_man;
	private int assassin;
	private int tank;

	public User(int id, int music, int sound, int adrenaline, int icecream, int supersnow, 
			int normal_ball, int slush, int ice, int normal_man, int assassin, int tank) {
		this.id = id;
		this.music = music;
		this.sound = sound;
		this.adrenaline = adrenaline;
		this.icecream = icecream;
		this.supersnow = supersnow;
		this.normal_ball = normal_ball;
		this.slush = slush;
		this.ice = ice;
		this.normal_man = normal_man;
		this.assassin = assassin;
		this.tank = tank;
	}

	public int getAdrenaline() {
		return adrenaline;
	}

	public int getAssassin() {
		return assassin;
	}

	public int getIce() {
		return ice;
	}

	public int getIcecream() {
		return icecream;
	}

	public int getId() {
		return id;
	}

	public int getMusic() {
		return music;
	}

	public int getNormal_ball() {
		return normal_ball;
	}

	public int getNormal_man() {
		return normal_man;
	}

	public int getSlush() {
		return slush;
	}

	public int getSound() {
		return sound;
	}

	public int getSupersnow() {
		return supersnow;
	}

	public int getTank() {
		return tank;
	}

	public void setAdrenaline(int adrenaline) {
		this.adrenaline = adrenaline;
	}

	public void setAssassin(int assassin) {
		this.assassin = assassin;
	}
	
	public void setIce(int ice){
		this.ice = ice;
	}

	public void setIcecream(int icecream) {
		this.icecream = icecream;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMusic(int music) {
		this.music = music;
	}

	public void setNormal_ball(int normal_ball) {
		this.normal_ball = normal_ball;
	}

	public void setNormal_man(int normal_man) {
		this.normal_man = normal_man;
	}

	public void setSlush(int slush) {
		this.slush = slush;
	}

	public void setSound(int sound) {
		this.sound = sound;
	}

	public void setSupersnow(int supersnow) {
		this.supersnow = supersnow;
	}

	public void setTank(int tank) {
		this.tank = tank;
	}
	
}