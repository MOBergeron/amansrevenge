package ca.qc.cvm.amansrevenge.entity;

public class Score {
	private int id;
	private int time;
	private int score;

	public Score(int id, int time, int score) {
		this.id = id;
		this.time = time;
		this.score = score;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}

