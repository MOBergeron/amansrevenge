package ca.qc.cvm.amansrevenge.sprite;

import org.andengine.input.touch.TouchEvent;


import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.ButtonOff;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class ButtonOn extends CVMSprite implements TouchAreaListener{
	private String mLink;
	
	public ButtonOn(float x, float y, String link){
		super(x, y, 120, 50, TextureManager.S_ON);
		mLink = link;
	}

	@Override
	public void onAreaTouched(TouchEvent touch, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) {

		if(touch.getAction() == TouchEvent.ACTION_UP){
			DataManager.getInstance().openDB();
			DataManager.getInstance().setOption(mLink, false);
			DataManager.getInstance().closeDB();
			
			scene.addSprite(new ButtonOff(getInitialX(), getInitialY(), mLink));
			scene.removeSprite(this);
		}
	}
}
