package ca.qc.cvm.amansrevenge.sprite;

import java.util.ArrayList;

import android.util.Log;

import ca.qc.cvm.amansrevenge.scene.GameScene;
import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.BallSprite.SnowType;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class HeroSprite extends CVMSprite implements ManagedUpdateListener, CollisionListener{
	public enum StateType {
		NORMAL (0, "Normal"),
		ADRENALINE (5, "Excited"),
		EXHAUSTED (3, "Exhausted"),
		INVINCIBLE (7, "Invincible"),
		BRAINFREEZE (3, "Brainfreeze"),
		FROZEN (3, "Frozen"),
		STUNNED (5, "Stunned");
		
		private final float eDuration;
		private final String eState;
		
		private StateType(float duration, String state){
			eDuration = duration;
			eState = state;
		}
		
		public final float getDuration(){
			return eDuration;
		}
		
		public final String getState(){
			return eState;
		}
	}
	
	private static final float HERO_SPEED_CAP = 68;
	private static final float BONUS_HEALTH = 20;
	private static final float MAX_TIMER = 5;
	private static final float POWER_UP_TIMER = 10;
	private static final float MAX_HEALTH = 100;
	private static final float MAX_BONUS = 10;
	
	private float mOldPosX;
	private float mOldPosY;
	private float mPosX;
	private float mPosY;
	private float mNextPosX;
	private float mNextPosY;
	private float mWalkSpeedX;
	private float mWalkSpeedY;
	private float mExhaustedTimer;
	private float mAdrenalineTimer;
	private float mInvincibleTimer;
	private float mBrainFreeze;
	private float mHealth;
	private float mThrowSpeed;
	private float mThrowTimer;
	private float mAngle;
	private float mWalkFactor;
	
	private int mBonusDmg;
	
	private boolean mStarted;
	private boolean mAlive;
	private boolean mFire;
	
	private StateType mState;
	private SnowType mSnowType;

	private ArrayList<Float> mIceCream;
	private ArrayList<Float> mAdrenaline;
	private ArrayList<Float> mSuperSnowTimer;
	
	public HeroSprite() {
		super((CVMGameActivity.CAMERA_WIDTH-50)/2, (CVMGameActivity.CAMERA_HEIGHT-30)/2, 50, 30, TextureManager.S_HERO);
		
		mPosX = super.getInitialX();
		mPosY = super.getInitialY();
		mWalkSpeedX = 0;
		mWalkSpeedY = 0;
		mHealth = MAX_HEALTH;
		
		mExhaustedTimer = 0;
		mAdrenalineTimer = 0;
		mInvincibleTimer = 0;
		mBrainFreeze = 0;
		mThrowTimer = 0;
		mWalkFactor = (float)3.25;
		
		mBonusDmg = 0;
		
		mStarted = false;
		mAlive = true;
		mFire = false;
		
		mIceCream = new ArrayList<Float>();
		mAdrenaline = new ArrayList<Float>();
		mSuperSnowTimer = new ArrayList<Float>();
		
		mSnowType = SnowType.NORMAL;	
		mThrowSpeed = mSnowType.getSpeed();
		mState = StateType.NORMAL;
	}
	
	public void restart(){
		mPosX = super.getInitialX();
		mPosY = super.getInitialY();
		mWalkSpeedX = 0;
		mWalkSpeedY = 0;
		mHealth = MAX_HEALTH;
		
		mExhaustedTimer = 0;
		mAdrenalineTimer = 0;
		mInvincibleTimer = 0;
		mBrainFreeze = 0;
		mThrowTimer = 0;
		
		mWalkFactor = (float)3.25;
		mBonusDmg = 0;

		mStarted = false;
		mAlive = true;
		mFire = false;
		
		if(DataManager.getInstance().isNormalBall()){
			mSnowType = SnowType.NORMAL;
		}
		else if(DataManager.getInstance().isSlush()){
			mSnowType = SnowType.SLUSH;
		}
		else if(DataManager.getInstance().isIce()){
			mSnowType = SnowType.ICE;
		}
		mThrowSpeed = mSnowType.getSpeed();
		mState = StateType.NORMAL;
		
		mIceCream.clear();
		mAdrenaline.clear();
		mSuperSnowTimer.clear();
	}
	
	public void move(float x, float y){
		if(x < -HERO_SPEED_CAP){
			x = -HERO_SPEED_CAP;
		}
		else if(x > HERO_SPEED_CAP){
			x = HERO_SPEED_CAP;
		}
		if(y < -HERO_SPEED_CAP){
			y = -HERO_SPEED_CAP;
		}
		else if(y > HERO_SPEED_CAP){
			y = HERO_SPEED_CAP;
		}
		mWalkSpeedX = x * mWalkFactor;
		mWalkSpeedY = y * mWalkFactor;
	}
	
	public void hit(float damage){
		if(mState != StateType.INVINCIBLE){
			if(mState == StateType.BRAINFREEZE){
				mHealth -= damage - 5;
			}
			else{
				mHealth -= damage;
			}
			
			if(mHealth <= 0){
				mAlive = false;
				SoundManager.getInstance().play(SoundManager.DIE);
				mHealth = 0;
			}
			
			SoundManager.getInstance().play(SoundManager.HURT);
		}
	}
	
	@Override
	public void collidedWith(CVMGameActivity activity, CVMAbstractScene scene,
			CVMSprite sprite) {
		if(sprite instanceof PowerUpSprite){
			switch(((PowerUpSprite) sprite).getType()){
			case ADRENALINE: 
				if(mState != StateType.INVINCIBLE){
					this.setState(StateType.ADRENALINE);
				}else{
					SoundManager.getInstance().play(SoundManager.ADRENALINE);
				}
				this.mAdrenalineTimer += mState.getDuration();
				this.mAdrenaline.add((float) POWER_UP_TIMER);
				((GameScene)scene).raiseCombo(3);
							
				break;
			case ICECREAM: 
				if(this.mHealth + BONUS_HEALTH <= MAX_HEALTH){
					this.mHealth += BONUS_HEALTH;
				}
				else{
					this.mHealth = MAX_HEALTH;
				}
				this.mIceCream.add((float) POWER_UP_TIMER);
				if(mIceCream.size() < 2){
					SoundManager.getInstance().play(SoundManager.ICECREAM);
				}
				break;
			case SUPERSNOW: 
				this.mBonusDmg += MAX_BONUS;
				this.mSuperSnowTimer.add(MAX_TIMER);
				SoundManager.getInstance().play(SoundManager.SUPERSNOW);
				((GameScene)scene).raiseCombo(2);
				break;
			}
			scene.removeSprite(sprite);
		}
		
	}

	@Override
	public void managedUpdate(float elaspedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		if(StateManager.getInstance().getState() == StateManager.PLAY){
			if(!mStarted){
				mStarted = true;
			}
			
			mAngle = this.getSprite().getRotation()+180;
			
			if(mPosX + mWalkSpeedX * elaspedSeconds > 80 && mPosX + mWalkSpeedX * elaspedSeconds < 2000 - this.getWidth()){
				mPosX += mWalkSpeedX * elaspedSeconds;
			}
			if(mPosY + mWalkSpeedY * elaspedSeconds > 0 && mPosY + mWalkSpeedY * elaspedSeconds < 1200 - this.getHeight()){
				mPosY += mWalkSpeedY * elaspedSeconds;
			}
			
			this.getSprite().setPosition(mPosX, mPosY);
			
			if(mFire){
				mFire = false;
				mThrowTimer+=elaspedSeconds;
				scene.addSprite(new BallSprite(mPosX, mPosY, mAngle, mSnowType, mBonusDmg));
				SoundManager.getInstance().play(SoundManager.THROW);
			}else{
				if(mThrowTimer != 0){
					mThrowTimer+=elaspedSeconds;
				}
				if(mThrowTimer >= mThrowSpeed){
					mThrowTimer = 0;
				}
			}
			
			powerUpTimeRemaining(elaspedSeconds, scene);
			
			powerUpTimer(elaspedSeconds, scene);
		}
		
		placeCamera(activity, scene);
	}

	private void powerUpTimer(float elaspedSeconds, CVMAbstractScene scene) {
		ArrayList<Float> tmpList;
		tmpList = mAdrenaline;
		for(int i = 0; i < tmpList.size(); i++){
			if(tmpList.get(i) <= 0){
				mAdrenaline.remove(i);
				((GameScene)scene).lowCombo(3);			
				}
			else{
				tmpList.set(i, tmpList.get(i) - elaspedSeconds);
			}
		}
		
		if(mAdrenaline.size()>= 3){
			mAdrenaline.remove(2);
			mAdrenaline.remove(1);
			mAdrenaline.remove(0);
			((GameScene)scene).lowCombo(9);
			if(mState != StateType.EXHAUSTED){
				setState(StateType.INVINCIBLE);
				mInvincibleTimer += mState.getDuration();
				((GameScene)scene).raiseCombo(15);
			}
		}
		
		tmpList = mIceCream;
		for(int i = 0; i < tmpList.size(); i++){
			if(tmpList.get(i) <= 0){
				mIceCream.remove(i);
			}
			else{
				tmpList.set(i, tmpList.get(i) - elaspedSeconds);
			}
		}
		
		if(mIceCream.size() >= 3){
			mIceCream.remove(2);
			mIceCream.remove(1);
			mIceCream.remove(0);
			if(mState != StateType.INVINCIBLE){
				setState(StateType.BRAINFREEZE);
				mBrainFreeze += mState.getDuration();
			}
		}
		
		tmpList = mSuperSnowTimer;
		for(int i = 0; i < tmpList.size(); i++){
			if(tmpList.get(i) <= 0){
				mSuperSnowTimer.remove(i);
				this.mBonusDmg -= MAX_BONUS;
				((GameScene)scene).lowCombo(2);
			}
			else{
				tmpList.set(i, tmpList.get(i) - elaspedSeconds);
			}
		}
	}

	private void powerUpTimeRemaining(float elaspedSeconds, CVMAbstractScene scene) {
		
		if(mExhaustedTimer > 0){
			mExhaustedTimer -= elaspedSeconds;
		}
		else{
			mExhaustedTimer = 0;
			if(mState == StateType.EXHAUSTED){
				setState(StateType.NORMAL);
			}
		}
		
		if(mBrainFreeze > 0){
			mBrainFreeze -= elaspedSeconds;
		}
		else{
			mBrainFreeze = 0;
			if(mState == StateType.BRAINFREEZE){
				setState(StateType.NORMAL);
			}
		}
		
		if(mAdrenalineTimer > 0){
			mAdrenalineTimer -= elaspedSeconds;
		}
		else{
			mAdrenalineTimer = 0;
			if(mState == StateType.ADRENALINE){
				setState(StateType.NORMAL);
			}
		}
		
		if(mInvincibleTimer > 0){
			mInvincibleTimer -= elaspedSeconds;
		}
		else{
			mInvincibleTimer = 0;
			if(mState == StateType.INVINCIBLE){
				setState(StateType.EXHAUSTED);
				mExhaustedTimer = mState.getDuration();
				((GameScene)scene).lowCombo(15);
			}
		}
	}
	
	private void placeCamera(CVMGameActivity activity, CVMAbstractScene scene) {
		float x = mPosX + getWidth()/2;
		float y = mPosY + getHeight()/2;
		
		if(x <=400){
			x = 400;
		}
		if(x >= 1600){
			x = 1600;
		}
		if(y <= 240){
			y = 240;
		}
		if(y >= 960){
			y = 960;
		}
		
		activity.getCamera().setCenter(x, y);
		((GameScene)scene).refreshText();
		((GameScene)scene).update();
	}

	public int getAdrenalineCount() {
		return mAdrenaline.size();
	}

	public float getHealth() {
		return mHealth;
	}

	public int getIceCreamCount() {
		return mIceCream.size();
	}

	public float getNextPosX() {
		return mNextPosX;
	}

	public float getNextPosY() {
		return mNextPosY;
	}

	public float getOldPosX() {
		return mOldPosX;
	}

	public float getOldPosY() {
		return mOldPosY;
	}

	public float getPosX() {
		return mPosX;
	}

	public float getPosY() {
		return mPosY;
	}

	public SnowType getSnowType() {
		return mSnowType;
	}

	public String getState() {
		return mState.getState();
	}

	public int getSuperSnowCount() {
		return mSuperSnowTimer.size();
	}

	public float getThrowTimer() {
		return mThrowTimer;
	}
	
	public boolean isAlive() {
		return mAlive;
	}

	public boolean isFire() {
		return mFire;
	}

	public void setFire(boolean mFire) {
		this.mFire = mFire;
	}

	public void setHealth(float mHealth) {
		this.mHealth = mHealth;
	}
	
	public void setNextPosX(float mNextPosX) {
		this.mNextPosX = mNextPosX;
	}

	public void setNextPosY(float mNextPosY) {
		this.mNextPosY = mNextPosY;
	}

	public void setOldPosX(float mOldPosX) {
		this.mOldPosX = mOldPosX;
	}

	public void setOldPosY(float mOldPosY) {
		this.mOldPosY = mOldPosY;
	}
	
	public void setPosX(float mPosX) {
		this.mPosX = mPosX;
	}

	public void setPosY(float mPosY) {
		this.mPosY = mPosY;
	}

	public void setSnowType(SnowType mSnowType) {
		this.mSnowType = mSnowType;
		if(mState != StateType.ADRENALINE && mState != StateType.INVINCIBLE){
			mThrowSpeed = mSnowType.getSpeed();
		}
		else{
			mThrowSpeed = mSnowType.getSpeed()/2;
		}
	}
	
	public void setState(StateType mState) {
		this.mState = mState;
		switch(mState){
		case ADRENALINE: 
			if(mAdrenaline.size() < 2){
				SoundManager.getInstance().play(SoundManager.ADRENALINE);
			}
			this.mThrowSpeed = mSnowType.getSpeed() / 2;
			this.mWalkFactor = (float)3.75;
			break;
		case EXHAUSTED: SoundManager.getInstance().play(SoundManager.EXHAUSTED);
			this.mThrowSpeed = mSnowType.getSpeed() * (float) 1.5;
			this.mWalkFactor = (float)2.75;
			break;
		case INVINCIBLE: SoundManager.getInstance().play(SoundManager.INVINCIBLE);
			this.mThrowSpeed = mSnowType.getSpeed() / 2;
			this.mWalkFactor = (float)3.75;
			break;
		case BRAINFREEZE: SoundManager.getInstance().play(SoundManager.BRAINFREEZE);
			this.mThrowSpeed = mSnowType.getSpeed() * (float) 1.5;
			break;
		default:
			this.mThrowSpeed = mSnowType.getSpeed();
			this.mWalkFactor = (float)3.25;
			break;
		}
	}

	public void setThrowTimer(float mThrowTimer) {
		this.mThrowTimer = mThrowTimer;
	}
	
}