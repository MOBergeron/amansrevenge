package ca.qc.cvm.amansrevenge.sprite;

import ca.qc.cvm.amansrevenge.scene.GameScene;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class BallSprite extends CVMSprite implements ManagedUpdateListener{
	public enum SnowType {
		NORMAL (35, (float)0.45, TextureManager.S_SNOWBALL),
		SLUSH (20, (float)0.3, TextureManager.S_SLUSHBALL),
		ICE (50, (float)0.75, TextureManager.S_ICEBALL);
		
		private final float eDamage;
		private final float eSpeed;
		private final int eTexture;
		
		private SnowType(float damage, float speed, int texture){
			eDamage = damage;
			eSpeed = speed;
			eTexture = texture;
		}
		
		public final float getDamage(){
			return eDamage;
		}
		
		public final float getSpeed(){
			return eSpeed;
		}
		
		public final int getTexture(){
			return eTexture;
		}
	}
	
	private float mPosX;
	private float mPosY;
	private float mSpeedX;
	private float mSpeedY;
	private float mDamage;
	private float mAngle;
	
	private boolean mStarted;
	
	private SnowType mType;
	
	public BallSprite(float x, float y, float angle, SnowType snowType, int bonus) {
		super(x, y, 12, 20, snowType.getTexture());
		mAngle = angle;
		mPosX = x;
		mPosY = y;
		mType = snowType;
		mDamage = snowType.getDamage() + bonus;
		mSpeedX = (float)(Math.sin((angle * Math.PI)/180)*300)*-1;
		mSpeedY = (float)Math.cos((angle * Math.PI)/180)*300;
		mStarted = false;
	}

	@Override
	public void managedUpdate(float elaspedSeconds, CVMGameActivity arg1,
			CVMAbstractScene scene) {
		if(StateManager.getInstance().getState() == StateManager.PLAY){
			if(!mStarted){
				mStarted = true;
				this.getSprite().setZIndex(-1);
				((GameScene)scene).sortChildren();
			}
			
			this.getSprite().setRotation(mAngle);
			mPosX += mSpeedX * elaspedSeconds;
			mPosY += mSpeedY * elaspedSeconds;
			boolean kill = false;
			if(mPosX < 0 || mPosX > 2000){
				kill = true;
			}
			if(mPosY < 0 || mPosY > 1200){
				kill = true;
			}
			
			if(kill){
				scene.removeSprite(this);
			}
			
			this.getSprite().setPosition(mPosX, mPosY);
		}
	}

	public float getPosX() {
		return mPosX;
	}

	public void setPosX(float mPosX) {
		this.mPosX = mPosX;
	}

	public float getPosY() {
		return mPosY;
	}

	public void setPosY(float mPosY) {
		this.mPosY = mPosY;
	}

	public float getDamage() {
		return mDamage;
	}

	public void setDamage(float mDamage) {
		this.mDamage = mDamage;
	}

	public SnowType getType() {
		return mType;
	}

	public void setType(SnowType mType) {
		this.mType = mType;
	}
}

