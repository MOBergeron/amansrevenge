package ca.qc.cvm.amansrevenge.sprite;

import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class SidePanelBGSprite extends CVMSprite{
	
	public SidePanelBGSprite() {
		super(0, 0, 80, 480, TextureManager.BG_SIDEPANEL);
	}
	
	public void update(CVMGameActivity activity){
		this.getSprite().setPosition(activity.getCamera().getXMin(), activity.getCamera().getYMin());
	}
}

