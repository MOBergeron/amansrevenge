package ca.qc.cvm.amansrevenge.sprite;

import org.andengine.entity.sprite.AnimatedSprite;

import ca.qc.cvm.amansrevenge.scene.GameScene;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class PowerUpSprite extends CVMSprite implements ManagedUpdateListener{
	public enum PowerUpType {
		ICECREAM (0.3, TextureManager.S_PU_ICECREAM),
		ADRENALINE (0.18, TextureManager.S_PU_ADRENALINE),
		SUPERSNOW (0.08, TextureManager.S_PU_SUPERSNOW);
		
		private final double eChance;
		private final int eTextureId;
		
		private PowerUpType(double chance, int textureId){
			eTextureId = textureId;
			eChance = chance;
		}
		
		public final double getChance(){
			return eChance;
		}
		public final int getTexture(){
			return eTextureId;
		}
	}
	
	private boolean mStarted;
	
	private PowerUpType mType;
	
	public PowerUpSprite(float x, float y, PowerUpType powerType) {
		super(x, y, 28, 28, powerType.getTexture());
		mType = powerType;
		mStarted = false;
	}
	
	@Override
	public void managedUpdate(float elaspedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		
		if(StateManager.getInstance().getState() == StateManager.PLAY){
			if(!mStarted){
				mStarted = true;
				this.getSprite().setZIndex(-1);
				((GameScene)scene).sortChildren();
				AnimatedSprite animate = (AnimatedSprite) this.getSprite();
				animate.animate(new long[]{250,250}, true);
			}
		}
		else{
			mStarted = false;
			AnimatedSprite animate = (AnimatedSprite) this.getSprite();
			animate.stopAnimation();
		}
	}

	public PowerUpType getType() {
		return mType;
	}

	public void setType(PowerUpType mType) {
		this.mType = mType;
	}
	
}

