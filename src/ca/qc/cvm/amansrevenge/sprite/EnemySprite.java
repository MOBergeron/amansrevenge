package ca.qc.cvm.amansrevenge.sprite;

import java.util.ArrayList;

import org.andengine.entity.sprite.AnimatedSprite;

import ca.qc.cvm.amansrevenge.scene.GameScene;
import ca.qc.cvm.amansrevenge.singleton.DataManager;
import ca.qc.cvm.amansrevenge.singleton.SoundManager;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.BallSprite.SnowType;
import ca.qc.cvm.amansrevenge.sprite.HeroSprite.StateType;
import ca.qc.cvm.amansrevenge.sprite.PowerUpSprite.PowerUpType;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class EnemySprite extends CVMSprite implements ManagedUpdateListener,
		CollisionListener {
	public enum EnemyType {
		NORMAL(36, 47, 50, 25, 125, 1, 5, TextureManager.S_SNOWMAN), 
		ASSASSIN(45, 36, 30, 25, 160, 0.5, 8, TextureManager.S_SNOWMAN_A), 
		TANK(40, 40, 100, 35, 50, 0.2, 10, TextureManager.S_SNOWMAN_T);

		private final int eWidth;
		private final int eHeight;
		private final float eHealth;
		private final float eDamage;
		private final float eSpeed;
		private final double eChance;
		private final int eScore;
		private final int eTextureId;

		private EnemyType(int width, int height, float health, float damage, float speed,
				double chance, int score, int textureId) {
			
			this.eWidth = width;
			this.eHeight = height;
			this.eHealth = health;
			this.eDamage = damage;
			this.eSpeed = speed;
			this.eChance = chance;
			this.eScore = score ;
			this.eTextureId = textureId;
		}

		public final int getWidth() {
			return eWidth;
		}

		public final int getHeight() {
			return eHeight;
		}

		public final float getHealth() {
			return eHealth;
		}

		public final float getDamage() {
			return eDamage;
		}

		public final float getSpeed() {
			return eSpeed;
		}

		public final double getChance() {
			return eChance;
		}
		
		public final int getScore() {
			return eScore;
		}

		public final int getTexture() {
			return eTextureId;
		}
	}

	private final float REDUCED_SPEED = 15;

	private final float REDUCED_SPEED_ASSASSIN = 25;
	private final float FROZEN_TIME_ASSASSIN = 5;

	private final float HIT_SPEED = 2;

	private float mPosX;
	private float mPosY;

	private float mHealth;
	private float mDamage;
	private float mSpeed;
	private float mHitTimer;

	private float mStunTimer;

	private boolean mEnableMovement;
	private boolean mFire;
	private boolean mStarted;

	private ArrayList<Float> mFrozenList;

	private HeroSprite mHero;
	private EnemyType mType;
	private StateType mState;

	public EnemySprite(HeroSprite hero, EnemyType type) {
		super(-100, -100, type.getWidth(), type.getHeight(), type.getTexture());
		mHero = hero;
		mType = type;
		mHealth = type.getHealth();
		mDamage = type.getDamage();
		mSpeed = type.getSpeed();

		mState = StateType.NORMAL;

		mStunTimer = 0;
		
		mFire = true;
		mEnableMovement = true;
		mStarted = false;

		mFrozenList = new ArrayList<Float>();
		
		spawn();
	}

	public void spawn() {
		if (Math.random() < 0.5) {
			mPosX = (float) (Math.random() * 2020) - 10;
			if (Math.random() < 0.5) {
				mPosY = -10;
			} else {
				mPosY = 1210;
			}
		} else {
			mPosY = (float) (Math.random() * 1220) - 10;
			if (Math.random() < 0.5) {
				mPosX = -10;
			} else {
				mPosX = 2010;
			}
		}
	}

	public PowerUpType gotLoot() {
		PowerUpType loot = null;
		double random = Math.random();

		if (random <= PowerUpType.ICECREAM.getChance()
				&& random > PowerUpType.ADRENALINE.getChance()) {
			if(DataManager.getInstance().isIcecream()){
				loot = PowerUpType.ICECREAM;
			}
		} else if (random <= PowerUpType.ADRENALINE.getChance()
				&& random > PowerUpType.SUPERSNOW.getChance()) {
			if(DataManager.getInstance().isAdrenaline()){
				loot = PowerUpType.ADRENALINE;
			}
		} else if (random <= PowerUpType.SUPERSNOW.getChance() && random >= 0) {
			if(DataManager.getInstance().isSupersnow()){
				loot = PowerUpType.SUPERSNOW;
			}
		}

		return loot;
	}

	public void drop(CVMAbstractScene scene) {
		PowerUpType powerup = gotLoot();
		if (powerup instanceof PowerUpType) {
			scene.addSprite(new PowerUpSprite(mPosX, mPosY, powerup));
		}
	}

	public void die(CVMAbstractScene scene) {
		SoundManager.getInstance().play(SoundManager.DIE);
		drop(scene);
		((GameScene)scene).setScore(mType.getScore());
		scene.removeSprite(this);
	}

	@Override
	public void collidedWith(CVMGameActivity activity,
			CVMAbstractScene scene, CVMSprite sprite) {
		if (sprite instanceof BallSprite) {
			mHealth -= ((BallSprite) sprite).getDamage();
			SnowType type = ((BallSprite) sprite).getType();

			switch (type) {
			case NORMAL:
				break;
			case SLUSH:
				setState(StateType.FROZEN);
				break;
			case ICE:
				setState(StateType.STUNNED);
				break;
			}

			if (mHealth < 0) {
				die(scene);
			} else {
				SoundManager.getInstance().play(SoundManager.TOUCHED);
			}

			scene.removeSprite(sprite);
		}
		
		if(sprite instanceof HeroSprite){
			if(mFire){
				mFire = false;
				((HeroSprite) sprite).hit(mDamage);
				mHitTimer = (float)0.001;
			}
		}
	}

	@Override
	public void managedUpdate(float elaspedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		if(StateManager.getInstance().getState() == StateManager.PLAY){
			if(!mStarted){
				mStarted = true;
				this.getSprite().setZIndex(-1);
				((GameScene)scene).sortChildren();
				if(mType != EnemyType.NORMAL){
					AnimatedSprite animate = (AnimatedSprite)this.getSprite();
					animate.animate(150, true);
				}
			}
			
			double radius = Math.atan2(mHero.getPosY()-this.mPosY, mHero.getPosX()-this.mPosX);
			double angle = (radius * 180) / Math.PI;
			
			this.getSprite().setRotation((float)angle+90);
			
			if(!mFire){
				if(mHitTimer != 0){
					mHitTimer+=elaspedSeconds;
				}
				if(mHitTimer >= HIT_SPEED){
					mHitTimer = 0;
					mFire = true;
				}
			}
			
			ArrayList<Float> tmpList;
			tmpList = mFrozenList;
			for (int i = 0; i < tmpList.size(); i++) {
				if (tmpList.get(i) <= 0) {
					mFrozenList.remove(i);
					if (mType == EnemyType.ASSASSIN) {
						if (mSpeed + REDUCED_SPEED_ASSASSIN <= mType.getSpeed()) {
							mSpeed += REDUCED_SPEED_ASSASSIN;
						} else {
							mSpeed = mType.getSpeed();
						}
					} else {
						if (mSpeed + REDUCED_SPEED <= mType.getSpeed()) {
							mSpeed += REDUCED_SPEED;
						} else {
							mSpeed = mType.getSpeed();
						}
					}
				} else {
					tmpList.set(i, tmpList.get(i) - elaspedSeconds);
				}
			}
	
			if (mEnableMovement) {
				ai(elaspedSeconds);
			} else {
				if (mStunTimer > 0) {
					mStunTimer -= elaspedSeconds;
				} else {
					mStunTimer = 0;
					if (mState == StateType.STUNNED) {
						setState(StateType.NORMAL);
					}
				}
			}
	
			this.getSprite().setPosition(mPosX, mPosY);
		}
		else{
			if(mStarted){
				mStarted = false;
				if(mType != EnemyType.NORMAL){
					AnimatedSprite animate = (AnimatedSprite)this.getSprite();
					animate.stopAnimation();
				}
			}
		}
	}

	public void ai(float elaspedSeconds) {
		if (this.mPosX < mHero.getPosX() - 15) {
			mPosX += mSpeed * elaspedSeconds;
		} else if (this.mPosX > mHero.getPosX() + 15) {
			mPosX -= mSpeed * elaspedSeconds;
		}

		if (this.mPosY < mHero.getPosY() - 15) {
			mPosY += mSpeed * elaspedSeconds;
		} else if (this.mPosY > mHero.getPosY() + 15) {
			mPosY -= mSpeed * elaspedSeconds;
		}
	}

	public void setState(StateType state) {
		mState = state;
		switch (state) {
		case FROZEN:
			if (mType == EnemyType.ASSASSIN) {
				if (mSpeed - REDUCED_SPEED_ASSASSIN >= 0) {
					mSpeed -= REDUCED_SPEED_ASSASSIN;
				} else {
					mSpeed = 0;
				}
				mFrozenList.add(FROZEN_TIME_ASSASSIN);
			} else {
				if (mSpeed - REDUCED_SPEED >= 10) {
					mSpeed -= REDUCED_SPEED;
				} else {
					mSpeed = 10;
				}
				mFrozenList.add(mState.getDuration());
			}
			break;
		case STUNNED:
			mEnableMovement = false;

			if (mType == EnemyType.TANK) {
				mStunTimer += (float) mState.getDuration() + 3;
			} else {
				mStunTimer += (float) mState.getDuration();
			}
			break;
		default:
			mEnableMovement = true;
			break;
		}
	}

}
