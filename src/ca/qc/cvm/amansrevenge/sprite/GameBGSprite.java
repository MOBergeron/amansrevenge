package ca.qc.cvm.amansrevenge.sprite;

import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;

public class GameBGSprite extends CVMSprite{
	
	public GameBGSprite() {
		super(0, 0, 2000, 1200, TextureManager.BG_GAME);
	}
}

