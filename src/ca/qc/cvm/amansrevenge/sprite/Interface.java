package ca.qc.cvm.amansrevenge.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.amansrevenge.singleton.SceneManager;
import ca.qc.cvm.amansrevenge.singleton.StateManager;
import ca.qc.cvm.amansrevenge.singleton.TextureManager;
import ca.qc.cvm.amansrevenge.sprite.BallSprite.SnowType;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class Interface extends CVMSprite implements TouchAreaListener{
	public enum InterfaceType {
		TITLE	((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 737, 81, SceneManager.NONE, TextureManager.S_TITLE),
		PLAY 	((CVMGameActivity.CAMERA_WIDTH-297)/2, 160, 297, 66, SceneManager.GAME, TextureManager.S_PLAY),
		OPTION 	((CVMGameActivity.CAMERA_WIDTH-383)/2, 260, 383, 67, SceneManager.OPTION, TextureManager.S_OPTION),
		SCORE 	((CVMGameActivity.CAMERA_WIDTH-362)/2, 360, 362, 66, SceneManager.SCORE, TextureManager.S_SCORE),
		CD1		((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 147, 71, SceneManager.NONE, TextureManager.T_CD1),
		CD2		((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 147, 71, SceneManager.NONE, TextureManager.T_CD2),
		CD3		((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 147, 71, SceneManager.NONE, TextureManager.T_CD3),
		CDGO	((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 147, 73, SceneManager.NONE, TextureManager.T_CDGO),
		T_PAUSE	((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 362, 66, SceneManager.NONE, TextureManager.T_PAUSE),
		GAMEOVER((CVMGameActivity.CAMERA_WIDTH-737)/2, 30, 494, 71, SceneManager.NONE, TextureManager.T_GAMEOVER),
		PAUSE 	(10, 10, 45, 45, SceneManager.NONE, TextureManager.S_PAUSE),
		NORMAL 	(0, 0, 45, 45, SceneManager.NONE, TextureManager.SP_SNOWBALL),
		SLUSH	(0, 0, 45, 45, SceneManager.NONE, TextureManager.SP_SLUSHBALL),
		ICE		(0, 0, 45, 45, SceneManager.NONE, TextureManager.SP_ICEBALL),
		ICECREAM(0, 0, 28, 28, SceneManager.NONE, TextureManager.S_PU_ICECREAM),
		ADRENALINE(0, 0, 28, 28, SceneManager.NONE, TextureManager.S_PU_ADRENALINE),
		SUPERSNOW(0, 0, 28, 28, SceneManager.NONE, TextureManager.S_PU_SUPERSNOW),
		MOVECONTROL (20, CVMGameActivity.CAMERA_HEIGHT-220, 150, 150, SceneManager.NONE, TextureManager.S_CONTROL),
		FIRECONTROL (CVMGameActivity.CAMERA_WIDTH-220, CVMGameActivity.CAMERA_HEIGHT-220, 150, 150, SceneManager.NONE, TextureManager.S_CONTROL);
		
		private final float x;
		private final float y;
		private final int width;
		private final int height;
		private final int sceneId;
		private final int textureId;
		private InterfaceType(float x, float y, int width, int height, int sceneId, int textureId){
			this.x=x;
			this.y=y;
			this.width=width;
			this.height=height;
			this.sceneId=sceneId;
			this.textureId=textureId;
		}
		public final float getX() {
			return x;
		}
		public final float getY() {
			return y;
		}
		public final int getWidth() {
			return width;
		}
		public final int getHeight() {
			return height;
		}
		public final int getSceneId() {
			return sceneId;
		}
		public final int getTextureId() {
			return textureId;
		}
	}
	private float mX;
	private float mY;
	
	private boolean mTouched;
	
	private InterfaceType mType;
	private HeroSprite mHero;
	
	public Interface(InterfaceType type, HeroSprite hero){
		super(type.getX(), type.getY(), type.getWidth(), type.getHeight(), type.getTextureId());
		mType = type;
		mHero = hero;
		mTouched = false;
	}

	@Override
	public void onAreaTouched(TouchEvent touch, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) {

		if(touch.getAction() == TouchEvent.ACTION_UP || touch.getAction() == TouchEvent.ACTION_CANCEL){
			switch(mType){
			case OPTION:
			case PLAY:
			case SCORE:
				activity.changeScene(mType.getSceneId());
				break;
			case NORMAL:
				mHero.setSnowType(SnowType.NORMAL);
				break;
			case SLUSH:
				mHero.setSnowType(SnowType.SLUSH);
				break;
			case ICE:
				mHero.setSnowType(SnowType.ICE);
				break;
			case PAUSE:
				if(StateManager.getInstance().getState() != StateManager.PAUSE && StateManager.getInstance().getState() != StateManager.END){
					StateManager.getInstance().setState(StateManager.PAUSE);
				}
				else if(StateManager.getInstance().getState() == StateManager.PAUSE)
				{
					StateManager.getInstance().setState(StateManager.WAIT);
				}
				break;
			case FIRECONTROL:
				mHero.setFire(false);
			case MOVECONTROL:
				mTouched = false;
				break;
			default:
				break;
			}
		}
		else{
			switch(mType){
			case FIRECONTROL:
				if(StateManager.getInstance().getState() == StateManager.PLAY){
					localX -= getWidth()/2;
					localY -= getHeight()/2;
					
					double radius = Math.atan2(localY, localX);
					double angle = (radius * 180) / Math.PI;
					
					mHero.getSprite().setRotation((float)angle+90);
					
					mTouched = true;
				}
				break;
			case MOVECONTROL:
				mTouched = true;
				mX = localX - getWidth()/2;
				mY = localY - getHeight()/2;
				break;
			default:
				break;
			}
		}
	}
	
	public void update(CVMGameActivity activity){
		switch(mType){
		case CD1:
		case CD2:
		case CD3:
		case CDGO:
			this.getSprite().setPosition(activity.getCamera().getXMin()+326, activity.getCamera().getYMin()+205);
			break;
		case T_PAUSE:
			this.getSprite().setPosition(activity.getCamera().getXMin()+219, activity.getCamera().getYMin()+207);
			break;
		case GAMEOVER:
			this.getSprite().setPosition(activity.getCamera().getXMin()+153, activity.getCamera().getYMin()+205);
			break;
		case ICECREAM:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+270);
			break;
		case ADRENALINE:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+335);
			break;
		case SUPERSNOW:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+400);
			break;
		case NORMAL:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+75);
			break;
		case SLUSH:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+140);
			break;
		case ICE:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+205);
			break;
		case PAUSE:
			this.getSprite().setPosition(activity.getCamera().getXMin()+10, activity.getCamera().getYMin()+10);
			break;
		case MOVECONTROL:
			this.getSprite().setPosition(activity.getCamera().getXMin()+70, activity.getCamera().getYMax()-170);
			if(mTouched){
				mHero.move(mX, mY);
			}
			else{
				mHero.move(0,0);
			}
			break;
		case FIRECONTROL:
			this.getSprite().setPosition(activity.getCamera().getXMax()-170, activity.getCamera().getYMax()-170);
			if(StateManager.getInstance().getState() == StateManager.PLAY){
				if(mTouched){
					if(mHero.getThrowTimer() == 0){
						mHero.setFire(true);
					}
				}
			}
			break;
		default:
			break;
		}
	}
}
