package ca.qc.cvm.amansrevenge.model;

import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.amansrevenge.entity.Score;
import ca.qc.cvm.amansrevenge.entity.User;
import ca.qc.cvm.amansrevenge.model.db.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Model {
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;

	public Model(Context ctx) {
		databaseHelper = new DatabaseHelper(ctx);
	}
	
	public long addScore(Score score){
		ContentValues values = new ContentValues();
		values.put("time", score.getTime());
		values.put("score", score.getScore());
		return database.insert("score", null, values);
	}
	
	public long changeValues(User user){
		ContentValues values = new ContentValues();
		values.put("music", user.getMusic());
		values.put("sound", user.getSound());
		values.put("adrenaline", user.getAdrenaline());
		values.put("icecream", user.getIcecream());
		values.put("supersnow", user.getSupersnow());
		values.put("normal_ball", user.getNormal_ball());
		values.put("slush", user.getSlush());
		values.put("ice", user.getIce());
		values.put("normal_man", user.getNormal_man());
		values.put("assassin", user.getAssassin());
		values.put("tank", user.getTank());
		return database.update("user", values,"id = 1",null);
	}
	
	public void closeDatabase() {
		database.close();
	}
	
	public void deleteScore(long line){
		database.delete("score","id = " + line, null);
	}
	
	public User fetchUser(){
		User user = null;
		Cursor cursor = database.query(true, "user", new String[]{"id", "music", "sound", "adrenaline", 
				"icecream", "supersnow", "normal_ball", "slush", "ice", "normal_man", "assassin", "tank"},
				null, null, null, null, null, null, null);
		
		if (cursor != null && cursor.moveToFirst()){
			user = new User(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), 
					cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9), cursor.getInt(10), cursor.getInt(11));
		}
		
		return user;
	}
	
	public List<Score> fetchScores(){
		List<Score> scoreList = new ArrayList<Score>();
		Cursor cursor = database.query(true, "score", new String[]{"id", "time" , "score"},
				null, null, null, null, "score DESC", null, null);
		int i = 0;
		if (cursor != null && cursor.moveToFirst()){
			do{
				scoreList.add(new Score(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2)));
				i++;
			}while(cursor.moveToNext() && i < 21); // Aller au prochain si != null
		}
		
		return scoreList;
	}
	
	public long[] getMusic(){
		Cursor cursor = database.query(true, "user", new String[]{"id", "music"},
				null, null, null, null, null, null, null);
		
		long[] list = new long[1];
		
		if (cursor != null && cursor.moveToFirst()){
			list[0] = cursor.getInt(1);
		}
		
		return list;
	}
	
	public Score getScore(long id){
		Cursor cursor = database.query(true, "score", new String[]{"id", "time" , "score"},
				"id = " + id, null, null, null, null, null, null);
		
		Score score = null;
		
		if (cursor != null){
			cursor.moveToFirst();
			
		}
		
		return score;
	}
	
	public long[] getSound(){
		Cursor cursor = database.query(true, "user", new String[]{"id", "sound"},
				null, null, null, null, null, null, null);
		
		long[] list = new long[1];
		
		if (cursor != null && cursor.moveToFirst()){
			list[0] = cursor.getInt(1);
		}
		
		return list;
	}
	
	public void openDatabase() {
		database = databaseHelper.getWritableDatabase();
	}
	
	public long setOption(String link, boolean value){
		ContentValues values = new ContentValues();
		values.put(link, value);
		return database.update("user", values,"id = 1",null);
	}
}

